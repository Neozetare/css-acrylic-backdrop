$(function() {
  // Pop out of flex flow, make draggable, make resizable

  const $container = $('#container');

  function freeExample(e) {
    e.preventDefault();
    const $el = $(e.currentTarget);

    $el.css({
      'height': $el.height() + 'px',
      'width': $el.width() + 'px',
      'top': 'calc(' + $el.offset().top + 'px + 1rem)',
      'left': 'calc(' + $el.offset().left + 'px + 1rem)'
    });
    $el.addClass('free');

    $container.append($el);
    $el.draggable({
      containment: "body",
      scroll: false,
      stack: '#container .example'
    });
    $el.resizable();

    $el.unbind('contextmenu', freeExample);
  }
  $('.example').contextmenu(freeExample);


  // Editable example

  const $editable = $('#editable');
  const $classnameParagraph = $('#editable-classname');

  const baseClassname = $editable.attr('class');

  console.log($editable, $classnameParagraph, baseClassname);

  function setClassName(className) {
    $editable.attr('class', baseClassname + ' ' + className);
    $editable.data('classname', className);
    $classnameParagraph.html(className.replace(/\s+/g, '<br>'));
  }
  setClassName($editable.data('classname'));

  $editable.dblclick(e => {
    const newClassName = prompt("Enter classes separated by spaces", $editable.data('classname'));
    if (newClassName != null) setClassName(newClassName);
  });
});